package org.name;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**ENTITY DEPARTEMENT**/
@Entity
public class Departement {

	@Column(name="ID")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name= "nom_departement" ,length=40) 
	private String NomDepartement;

	//Un departement peut avoir plusieurs communes
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinFetch(JoinFetchType.OUTER)
	//private Commune commune;
	private Set<Commune> commune =new TreeSet<Commune>(new Comparator<Commune>() {

		public int compare(Commune commune1, Commune commune2)
		{
			if(commune1.getNomCommune().equals(commune2.getNomCommune()))
			{
				return commune1.getCodePostal().compareTo(commune2.getCodePostal());
			}
			else 
			{
				return commune1.getNomCommune().compareTo(commune2.getNomCommune());
			}
		}
	});


	//GETTER & AJOUT COMMUNES (Defency copy)
	public Set<Commune> getCommunes() {

		Set<Commune> commune1 =new TreeSet<Commune>(new Comparator<Commune>() {

			public int compare(Commune c1, Commune c2)
			{
				if(c1.getNomCommune().equals(c2.getNomCommune()))
				{
					return c1.getCodePostal().compareTo(c2.getCodePostal());
				}
				else 
				{
					return c1.getNomCommune().compareTo(c2.getNomCommune());
				}
			}
		});

		commune1.addAll(commune);
		return commune1;
	}


	//Ajout d'une commune
	public void AddCommune(Commune comm)
	{
		if(comm.getDepartement()!=null)
			this.commune.add(comm);
	}


	//GETTER MAIRE (defency copy)
	public Maire getMaire(Commune comm)
	{
		Iterator<Commune> iterateur=commune.iterator();

		while(iterateur.hasNext())
		{
			Commune commune3=iterateur.next();

			if(commune3.equals(comm))
				return commune3.getMaire();
		}

		return null;
	}


	public List <Maire> getMaires(){
		List<Maire> maires = new ArrayList<Maire>();

		Iterator<Commune> iterator = this.getCommunes().iterator();
		while(iterator.hasNext()) {
			maires.add(iterator.next().getMaire());
		}
		return maires;
	}

	//GETTER & SETTERS ID
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	//GETTER & SETTERS DEPARTEMENT
	public String getNomDepartement() {
		return NomDepartement;
	}

	public void setNomDepartement(String nomDepartement) {
		NomDepartement = nomDepartement;
	}

	//AFFICHAGE
	@Override
	public String toString() {
		return "Departement [id=" + id + ", Nom du Departement=" + NomDepartement
				+ ", la commune=" + commune + "]";
	}
}
